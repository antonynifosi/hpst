# HarryPotterSpellThrower

Pour jouer à notre jeu allez sur le site : https://antonynifosi.gitlab.io/hpst

Le projet est disponible à cette adresse : https://gitlab.com/antonynifosi/hpst

Les données de la base de donnée sont dans le fichier db.sql dans l’archive .zip

L’archive contient deux dossiers : 
- Un concernant le projet visual studio de l’API du jeu qui est maintenant hébergée sur Azure
- Un concernant le front du projet, qui est un projet Angular Material maintenant hébergé sur GitLab

Si vous souhaitez lancer le tout en local il suffit de lancer d’abord l'API avec le projet Visual Studio puis de lancer dans le dossier du front une console de commande et taper:
```sh
npm install -g @angular/cli
npm install
ng serve 
```

Une page Aide permet aux nouveaux joueurs de savoir ce qu'il faut faire (le principe est très simple, vaincre les montres en lançant des sorts pour passer de niveau).

Si vous avez toutes autres questions n'hésitez pas à contacter un des membres de cette formidable équipe !

En espérant que vous vous amuserez bien !

Armen Aristakessyan - Florian Bouchut - François Pouly - Antony Nifosi